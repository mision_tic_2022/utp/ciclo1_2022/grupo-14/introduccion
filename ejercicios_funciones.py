'''
1)Desarrolle una función que reciba como parámetro el nombre del usuario y la ciudad. 
La función deberá mostrar en pantalla:
    -> Hola nombre_usuario nos alegra que nos visites desde nombre_ciudad
2) Desarrolle una función que calcule y muestre el promedio de 4 notas de un estudiante
3) Desarrolle una función que muestre los intereses a ganar de un CDT:
    FORMULA PARA CALCULAR LOS INTERESES: (cantidad_dinero * porcentaje_interes * tiempo) / 12
4) Desarrolle una función que eleve a la potencia un número
5) Desarrolle una función que calcule y muestre el total de dinero ganado en un CDT: 
    interes ganado = (cantidad_dinero * porcentaje_interes * tiempo) / 12
    NOTA:
    El porcentaje del interes debe recibirlo como un entero y luego convertirlo a float,
    por ejemplo; 
    30 -> 0.3
    50 -> 0.5
    15 -> 0.15
'''

#Punto 1
def saludo(usuario: str, ciudad):
    print(f"Hola {usuario} nos alegra que nos visites desde {ciudad}")

saludo('Andrés', 'Medellín')

#----------------------------Punto 2----------------------------------
#Solución de Carlos Eduardo
def promedioNota(nota1,nota2,nota3,nota4):
    promedio = (nota1 + nota2 + nota3 + nota4) /4
    print("su promedio es de ", promedio)

#Solución de Luis Alejandro
def promedio_estudiantes(nota_1, nota_2, nota_3, nota_4):
    promedio=(nota_1+nota_2+nota_3+nota_4)/4.0
    print(f"El promedio de {nota_1},{nota_2},{nota_3},{nota_4} es {promedio}")

#Solución de Manuel Santiago
def promedio(nota1, nota2, nota3, nota4):
    prom = (nota1 + nota2 + nota3 + nota4)/4
    print(f"El promedio es: {prom}")

#-----------------Punto 4-----------------
#Solución de Luis Alejandro
def INTERESES(dinero, porcen_int, tiempo):
    INTER= (dinero*porcen_int* tiempo)/12
    print(f"Los intereses de {dinero} con {porcen_int} por {tiempo} meses es {INTER}")

#------------------Punto 5 --------------------
#Solución de Luis Alejandro
def Dinero_total_ganado(cantidad_dinero,porcentaje_intereses,tiempo):
    intereses=(cantidad_dinero*(porcentaje_intereses/100)*tiempo)/12
    Dinero_total=cantidad_dinero+intereses
    print(f"El dinero ganado en un CDT con inversion inicial: {cantidad_dinero}, interes: {porcentaje_intereses}%, durante: {tiempo} meses es: {Dinero_total}")

Dinero_total_ganado(12000,30,15)
