
def sumar(n1, n2):
    suma = n1 + n2
    return suma

def multiplicacion(n1, n2):
    mult = n1 * n2
    return mult

#resp1 = sumar(10, 50)
#resp2 = sumar(20, 30)
#mult = multiplicacion(resp1, resp2)
#print(mult)

#Función que retorna un String
def saludar(nombre_usuario: str, ciudad: str):
    return f"¡Hola {nombre_usuario}! nos alegra que nos visites desde {ciudad}"

print( saludar('Yeyson', 'Cartagena') )
