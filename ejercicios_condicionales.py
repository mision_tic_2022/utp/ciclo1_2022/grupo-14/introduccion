'''
1) Desarrollar una función que reciba como parámetro el nombre y edad de una persona,
    retorne un String que indique si la persona es mayor de edad o menor de edad
    Ejemplo:
        'Pedro Perez es mayor de edad'
        ó 'Pedro Perez es menor de edad'

2) Desarrollar una función que retorne una serie de ejercicios al usuario, la función debe
    recibir como parámetro un String.
    facil:
        10 flexiones de pecho
        10 sentadillas
    medio:
        50 flexiones de pecho
        10 burpes
        50 sentadillas
    avanzado:
        100 flexiones de pecho con palmada
        80 paracaidistas
        200 abdomilaes
    NOTA:
        \n -> Salto de linea
        \t -> Tabular
'''
#Solución de Carlos eduardo
def punto1 (nombre_persona, edad_persona):
    if edad_persona >= 18:
        return f'{nombre_persona}, es mayor de edad'
    else:
        return f'{nombre_persona}, es menor de edad'
    
#print(punto1('Eduardo', 22))

#Solución de Adela León
def ejercicio(palabra):
    ejercicios = ''
    if palabra == "facil":
        ejercicios = "10 flexiones de pecho \n 10 sentadillas"
    elif palabra == "medio":
        ejercicios = "50 flexiones \n 10 burpes \n 10 sentadillas"
    elif palabra == "dificil":
        ejercicios = "100 flexiones \n 80 paracaidas \n 100 abdominales"
    return ejercicios

#Solución de Fabián Urrea
def ejercicios(dificultad):
    if dificultad=='facil':
        return """facil: 
        10 flexiones de pecho   
        10 sentadillas"""
    elif dificultad=='medio':
        return """medio: 
    50 fexiones de pecho
    10 burpes
    50 sentadillas"""
    elif dificultad=='avanzado':
        return """avanzado: 
    100 flexiones de pecho con palmada
    80 paracaidistas
    200 abdominales"""


def seleccion(dificultad):
    if dificultad == "facil":
        return "10 flexiones \n 10 sentadillas"
    elif dificultad == "medio":
        return "25 flexiones \n10 burpes\n50 sentadillas"
    elif dificultad == "dificil":
        return " 50 flexiones \n 25 sentadillas"
    else:
        return "seleccion no valida"

'''
3) Desarrollar una función que retorne una cadena con el valor del interes y 
el total del dinero a retirar de un CDT.
    * Si el usuario retira el dinero igual o menor a dos meses se 
        aplica una penalidad del 2%(0.02)
        valor_penalidad = dinero_invertido * 0.02
    *Si el usuario retira el dinero mayor a dos meses recibirá un interés sobre su dinero
        valor_interes = (dinero * porcentaje_interes * tiempo) / 12
    
    NOTA:
        La función deberá recibir como parámetro: dinero, porcentaje_interes, tiempo
    
    Ejemplo de la cadena que debe retornar si no aplica penalidad:
        "*El valor del interés es de {valor_interes} 
         *El total del dinero a retirar es {total_dinero}"
    Si aplica penalidad:
        "*EL valor de la penalidad es de {valor_penalidad}
        *El total del dinero a retirar es {total_dinero}"


'''

#Solución de Luis Alejandro
def interes_cdt(dinero, porcentaje_interes,tiempo):
    if(tiempo>2):
        valor_interes=(dinero*porcentaje_interes*tiempo)/12
        total_dinero=dinero+valor_interes
        mensaje=f"*El valor total de los intereses es de {valor_interes}\n*El valor total del dinero a retirar es {total_dinero}"
    else:
        valor_penalidad=dinero*0.02
        total_dinero=dinero-valor_penalidad
        mensaje=f"*El valor de la penalidad es de {valor_penalidad}\n*El total del dinero a retirar es {total_dinero}"
    return mensaje


mensaje = "Hola\nmundo"
print(mensaje)