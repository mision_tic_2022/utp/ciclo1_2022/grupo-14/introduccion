
#Booleanos
p = True
q = False
r = True

#Negado de toda la expresión
if not(p and r):
    print('Es verdad la condición AND')
else:
    print('Es falsa la condición AND')

#Negado de p
if not p or q:
    print('Es verdad la condición OR')
else:
    print('Es falso la condición OR')

num_1 = 100
num_2 = 50
num_3 = 200
num_4 = 50

if num_1 > num_2: 
    print('num_1 es mayor a num_2')
    #Condición anidada
    if(num_1 > num_3):
        print('num_1 es mayor a num_3')
    else:
        print('num_1 NO es mayor a num_3')
else:
    print('num_1 NO es mayor a num_2')

'''
Un solo igual (=) es asignación
Un doble igual (==) es comparación
'''
if num_1 == num_2:
    print('num_1 es igual a num_2')
else:
    print('num_1 es diferente a num_2')

if num_3 != num_4:
    print('num_3 es diferente a num_4')
else:
    print('num_3 es igual a num_4')

'''
num_1 = 100
num_2 = 50
num_3 = 200
num_4 = 50
'''

if num_1 > num_2 and num_3 < num_1 or num_4 > num_2 and num_2 < num_3:
    print('Cumple la condición')
else:
    print('No cumple la condición')

if num_1 > num_2:
    print('es mayor')
elif num_1 == num_2:
    print('num_1 es igual a num_2')
else:
    print('num_1 es diferente a num_2')

