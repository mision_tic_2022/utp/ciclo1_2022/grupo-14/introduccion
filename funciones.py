'''-----------FUNCIONES---------'''

#def -> Palabra reservada para crear una función

#Crear función
def miPrimeraFuncion():
    mensaje = "Hola mundo, esta es mi primera función en python"
    print(mensaje)

#Función sin parámetros
def suma():
    num_1 = 10
    num_2 = 20
    suma = num_1 + num_2
    print(f"{num_1} + {num_2} = {suma}")

#Función con parámetros
#El tipado se indica ÚNICAMENTE por legibilidad de código
def suma_con_parametros(num_1: float, num_2: float, num_3: float):
    resultado = num_1 + num_2 + num_3
    print(f"{num_1} + {num_2} + {num_3} = {resultado}")

#Llamar funciones
#miPrimeraFuncion()
#suma()
suma_con_parametros(10, 5, 20)
suma_con_parametros(50, 2, 80)