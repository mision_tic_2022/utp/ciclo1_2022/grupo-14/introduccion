
fruta = 'Durazno'
tamanio = len(fruta)
print(f'El número de caracteres es: {tamanio}')

ultimo_caracter = fruta[tamanio-1]
print(ultimo_caracter)

'''
Utilizo indices negativos para acceder a los caracteres de
atrás hacia adelante
'''
print(fruta[-2])

#Rebanando cadenas
rebanado_1 = fruta[0:2]
print(rebanado_1)

rebanado_2 = fruta[1:5]
print(rebanado_2)