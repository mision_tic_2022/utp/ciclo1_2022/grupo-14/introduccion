#Esto es un comentario de una sola linea
"""
Esto es un comentario
de varias
lineas
"""

'''
Esto es otro comentario
de varias 
lineas
'''

#Mostrar mensaje en consola
print("Hola mundo")

#Variables
#Enteros, es decir, no tiene decimales
numero: int = 10 #Poner el tipo de dato ÚNICAMENTE funciona como legibilidad del código
miVariable = 20
num_1 = 5
num_2 = 10
#Flotantes, es decir, contiene decimales
num_3 = 15.24
num_4: float = 20.10
#String, es decir, texto
texto_1 = 'Esto es un texto, puede ser comillas dobles o comillas simples'
texto_2: str = "Esto es otro texto, es decir, un String"

#Operaciones
suma = num_1 + num_2
resta = num_3 - num_4
multiplicacion = num_1 * num_3
division = suma / num_1
#Doble 'signo de multiplicación' refiere a un elevado a la potencia
elevado = num_1 ** 2 
#Módula, retorna el residuo de una división
modulo = num_2 % 3

formula = ( (num_1 + num_2) * num_3 ) / (num_4 ** 2)

'''
Mostrar datos en consola
'''
#Concatenar en el PRINT con 'coma'
print('La suma de num_1 y num_2 = ', suma)
#Formatar la salida restringiendo la cantidad de decimales a mostrar
print("Resta: num_3 - num_4 = %.4f" %resta)
print(f"División: suma / {num_1} = {division}")

print(texto_1)
print(texto_2)
concatenar = texto_1 + ' '+ texto_2
print(concatenar)


